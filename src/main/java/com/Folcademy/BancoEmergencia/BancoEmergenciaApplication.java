package com.Folcademy.BancoEmergencia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoEmergenciaApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoEmergenciaApplication.class, args);
	}

}
